module.exports = (Host, generateName) => {
	const storeAddressAndName = body => {
		return Host.create({ ip: body.ip, name: body.domain, generated_domain: generateName(), protocol: 'http://' });
	};

	const getSite = subDomain =>{
		return Host.find({
			generated_domain: subDomain
		});

	}
	return {
		storeAddressAndName, getSite
	};
};