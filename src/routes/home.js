const homeController = require('../controllers/home');

module.exports = app => {
	controller = homeController(app.models.host, app.helpers.generateName);
	const request = app.helpers.request;

	app.get('/', (req, res) => {

		let domain = req.headers.host,
		    subDomain = domain.split('.');
		if(subDomain.length > 2)
		{		
			console.log(subDomain[0])
				controller.getSite(subDomain[0])
				.then(data => {
					console.log(data[0].name)
					var data = data[0];
					var protocoluri = data.protocol + data.name;
					var re = new RegExp(protocoluri, "gi");
					var ip = new RegExp(data.ip, "gi");
					var href = /<href="(.*?)">/gi;
					//var href2 = /'([^'']+.)/gi;
					request(protocoluri, (error, response, body) => {
						 console.log(domain)
					  	console.log('error:', error);
					  	console.log('statusCode:', response && response.statusCode);
					  	
					  	body = body.replace(re, domain);
					  	body = body.replace(ip, domain);
					  	body = body.replace(href, 'http://'+domain)
					  	//body = body.replace(href2, '"' + domain + '"')
					  	
					  	res.send(body);
				  	});
				})
				.catch(error => {
					res.status(500).send(`Whoops something broke ${error}`);
				});
		}
		else
		{
			res.render('home/index');
		}

	});

	app.get('/*', (req, res, next) => {
		let domain = req.headers.host,
		    subDomain = domain.split('.');
		 var route = req.params[0];		 
		if(subDomain.length > 2){
				controller.getSite(subDomain[0])
				.then(data => {
					var data = data[0];
					var protocoluri = data.protocol + data.name;
					var re = new RegExp(protocoluri, "gi");
					var ip = new RegExp(data.ip, "gi");
					var href = /<href="(.*?)">/gi;
					//var href2 = /'([^'']+.)/gi;
					request(protocoluri+'/'+route, (error, response, body) => {
					  	console.log('error:', error); // Print the error if one occurred
					  	console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
					  	body = body.replace(re, domain);
					  	body = body.replace(ip, domain);
					  	body = body.replace(href, 'http://'+domain)
					  	//body = body.replace(href2, '"' + domain + '"')
					  	res.send(body);
				  	});
				})
				.catch(error => {
					res.status(500).send(`Whoops something broke ${error}`);
				});
		}
		
	})
	app.post('/*', (req, res, next) => {
		console.log(req.body)
		let domain = req.headers.host,
		    subDomain = domain.split('.');
		 var route = req.params[0];		 
		if(subDomain.length > 2){
				controller.getSite(subDomain[0])
				.then(data => {
					var data = data[0];
					var protocoluri = data.protocol + data.name;
					var re = new RegExp(protocoluri, "gi");
					var ip = new RegExp(data.ip, "gi");
					var href = /href="(.*?)")/gi;
					//var href2 = /'([^'']+.)/gi;
					/*request(protocoluri+'/'+route, (error, response, body) => {
					  	console.log('error:', error); // Print the error if one occurred
					  	console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
					  	body = body.replace(re, domain);
					  	body = body.replace(ip, domain);
					  	body = body.replace(href, domain)
					  	res.send(body);
				  	});*/
				  	var post = req.body;
					request.post({
					  headers: {'content-type' : 'application/x-www-form-urlencoded'},
					  url:     protocoluri+'/'+route,
					  body:    post,
					  json: true,
					}, function(error, response, body){
						console.log('error:', error); // Print the error if one occurred
					  	console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
					  	body = body.replace(re, domain);
					  	body = body.replace(ip, domain);
					  	body = body.replace(href, 'http://'+domain)
					  ;
					  res.send(body);
					});
				})
				.catch(error => {
					res.status(500).send(`Whoops something broke ${error}`);
				});
		}
		
	})

	app.post('/', (req, res, next) => {
		console.log(req.body)
		let domain = req.headers.host,
		    subDomain = domain.split('.');
		 var route = req.params[0];		 
		if(subDomain.length > 2){
				controller.getSite(subDomain[0])
				.then(data => {
					var data = data[0];
					var protocoluri = data.protocol + data.name;
					var re = new RegExp(protocoluri, "gi");
					var ip = new RegExp(data.ip, "gi");
					var href = /href="(.*?)"/gi;
				  	var post = req.body;
					request.post({
					  headers: {'content-type' : 'application/x-www-form-urlencoded'},
					  url:     protocoluri+'/'+route,
					  body:    post,
					  json: true,
					}, function(error, response, body){
						console.log('error:', error); // Print the error if one occurred
					  	console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
					  	body = body.replace(re, domain);
					  	body = body.replace(ip, domain);
					  	body = body.replace(href, 'http://'+domain)
					  	//body = body.replace(href2, '"' + domain + '"')
					  ;
					  res.send(body);
					});
				})
				.catch(error => {
					res.status(500).send(`Whoops something broke ${error}`);
				});
		}
		
	})


	app.post('/create', (req, res) => {
		controller.storeAddressAndName(req.body)
			.then(data => {
				app.helpers.debug(data);
				res.redirect('/');
			})
			.catch(error => {
				res.status(500).send(`Whoops something broke ${error}`);
			});
	});
};