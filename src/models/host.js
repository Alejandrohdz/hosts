module.exports = Waterline => {
	return Waterline.Collection.extend({
		identity: 'host',
		connection: 'default',

		attributes: {
			id: {
				type: 'integer',
				primaryKey: true,
				autoIncrement: true,
				unique: true
			},
			ip: {
				type: 'string',
				required: true
			},
			name: {
				type: 'string',
				required: true
			},
			generated_domain: {
				type: 'string',
				required: true
			},
			protocol:{
				type: 'string',
				required: true
			}
		}
	});
};