const fsHelpers = require('../util/fs-helpers.js');

const extractRoutes = files => {
	return files.map(file => require(file));
};

module.exports = app => {
	let files = fsHelpers.extractFiles(__dirname, '../routes');
	let routes = extractRoutes(files);

	routes.forEach(route => {
		console.log(route(app));
		route(app);
	});
};