const Waterline = require('waterline');
const mysqlAdapter = require('sails-mysql');
const fsHelpers = require('../util/fs-helpers.js');

const {
	MYSQL_HOST,
	MYSQL_USER,
	MYSQL_PASS,
	MYSQL_NAME,
	MYSQL_TEST_HOST,
	MYSQL_TEST_USER,
	MYSQL_TEST_PASS,
	MYSQL_TEST_NAME,
	NODE_ENV
} = process.env;

const waterline = new Waterline();

const addModels = files => {
	files.forEach(file => {
		let item = require(file)(Waterline);
		waterline.loadCollection(item);
	});
};

const getConnection = () => {
	return {
		adapters: {
			mysql: mysqlAdapter
		},
		connections: {
			default: {
				adapter: 'mysql',
				host: NODE_ENV === 'TEST' ? MYSQL_TEST_HOST : MYSQL_HOST,
				user: NODE_ENV === 'TEST' ? MYSQL_TEST_USER : MYSQL_USER,
				password: NODE_ENV === 'TEST' ? MYSQL_TEST_PASS : MYSQL_PASS,
				database: NODE_ENV === 'TEST' ? MYSQL_TEST_NAME : MYSQL_NAME
			}
		}
	};
};

module.exports = () => {
	const config = getConnection();
	let models = null;

	let files = fsHelpers.extractFiles(__dirname, '../models');
	addModels(files);

	// Should seed database

	return new Promise((resolve, reject) => {
		waterline.initialize(config, (err, models) => {
			if(err) return reject(err);
		
			resolve(models);
		});
	});
};