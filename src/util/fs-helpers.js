const fs = require('fs');
const path = require('path');

const extractFiles = (dirname, folder) => {
	let folderPath = path.join(dirname, folder);
	let folderItems = fs.readdirSync(folderPath);

	return folderItems
		.map(item => path.join(folderPath, item))
		.filter(itemPath => fs.statSync(itemPath).isFile());
};

module.exports = { extractFiles };