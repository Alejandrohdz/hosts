const debug = require('debug');
const generateName = require('./name-generator.js');
const request = require('request');

module.exports = {
	debug: debug('http'),
	error: debug('error'),
	generateName,
	request
};