const HomeController = require('../../../src/controllers/home');
const generateName = require('../../../src/util/name-generator.js');

describe('Controller: home', () => {
	const ip = '127.0.0.1';
	const name = 'example.com';
	const generated_domain = generateName();

	const Host = {
		create: td.function()
	};

	const mockHost = {
		ip,
		name,
		generated_domain
	};

	describe('Create', () => {
		it('should create a host entry', () => {
			let newHost = JSON.parse(JSON.stringify(mockHost));

			td.when(HomeController.create(newHost)).thenResolve(mockHost);

			HomeController(Host, generateName).storeAddressAndName(newHost)
				.then(host => {
					expect(host).to.exist;
				})
				.catch(error => {
					expect(error).to.not.exist;
				})
		});
	});
});