module.exports = app => {
	const port = process.env.PORT || 3031;

	app.listen(port, () => {
		app.helpers.debug('Listening on port', port);
		app.helpers.debug('ENV:', process.env.NODE_ENV);
	});
};