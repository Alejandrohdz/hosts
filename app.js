require('dotenv').config();

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const helpers = require('./src/util/express-helpers.js');
const routes = require('./src/config/routes');
const datasource = require('./src/config/datasource');
const startServer = require('./index.js');

const app = express();
// console.log();
app.set('views', path.join(__dirname, 'src/views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'src/public')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

app.helpers = helpers;

datasource()
	.then(models => {
		app.models = models.collections;
		routes(app);

		startServer(app);
	})
	.catch(error => {
		app.helpers.error(error);
	});